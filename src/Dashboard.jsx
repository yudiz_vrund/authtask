import React, { useState, useEffect } from 'react'
import { Modal, Table,ModalBody } from 'reactstrap';
import axios from 'axios';
import './index.css';
import Pagin from './Pagin';
import { useHistory } from 'react-router-dom';


const Dashboard = () => {

    const [sports, setSports] = useState([]);

    // eslint-disable-next-line no-unused-vars
    const [currentPage, setCurrentPage] = useState(1);

    // eslint-disable-next-line no-unused-vars
    const [sportsPerPage, setSportsPerPage] = useState(3);

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    const history = useHistory();
    
    const [title,setTitle] = useState();
    const [desc,setDesc] = useState();
    const [pic,setPic] = useState();

    useEffect(() => {
        const getSports = async () => {
            const res = await axios.get('https://backend.sports.info/api/v1/posts/recent');
            setSports(res.data.data);
        }

        getSports();

    }, [])

    const indexOfLastSport = currentPage * sportsPerPage;
    const indexOfFirstSport = indexOfLastSport - sportsPerPage;
    const currentSports = sports.slice(indexOfFirstSport, indexOfLastSport)

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    

    const openModel = (title,desc,img) =>{
        setModal(true);
        setTitle(title);
        setDesc(desc);
        setPic(img);
    }

    const handleLogout = () =>{
        localStorage.clear();
        history.replace("/");
    }

    return (
        <>
            <Table bordered>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>View Count</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody>
                    {currentSports.map(sport => (
                        <tr key={sport._id} onClick={() => openModel(sport.sTitle,sport.sDescription,sport.sImage)}>
                            <td>{sport.sTitle}</td>
                            <td>{sport.nViewCounts}</td>
                            <td><img src={sport.sImage} height={100} width={100}></img></td>
                        </tr>

                    ))}
                </tbody>
            </Table>
            <Pagin sportsPerPage={sportsPerPage} totalSports={sports.length} paginate={paginate} />

            <Modal isOpen={modal} toggle={toggle}>
                <ModalBody>
                        <h5>{title}</h5> 
                        <br/>
                        <img src={pic} width="400" height="300"></img>
                        <br/>
                        {desc}
                </ModalBody>
            </Modal>

            <button onClick={handleLogout}>Logout</button>
            {/* {sports.map(sport => (
                <li key={sport._id}>
                    {sport.sTitle}
                </li>
            ))} */}
        </>
    )
}

export default Dashboard;