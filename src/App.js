import React from 'react';
import { BrowserRouter} from 'react-router-dom';
import CustomRoute from "./Route";

function App() {
  return (
    <>
      <BrowserRouter>
        <CustomRoute />
      </BrowserRouter>
    </>
  );
}

export default App;
