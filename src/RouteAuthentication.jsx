import React from "react";
import { Route, Switch } from "react-router-dom";
import Dashboard from "./Dashboard";

const RouteAuthentication = () => {
  return (
    <Switch>
      <Route exact  path="/dashboard" component={Dashboard} />
    </Switch>
  );
};
export default React.memo(RouteAuthentication); 