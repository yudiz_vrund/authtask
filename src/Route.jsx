/* eslint-disable no-unused-vars */
import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import RouteAuthentication from "./RouteAuthentication";
import Login from "./Login";

// eslint-disable-next-line react/prop-types
const AuthRoute = ({ component: Component, ...rest }) => {

    const authUser = localStorage.getItem("userData");

    return (
        <Route
            {...rest}
            render={(props) =>
                authUser ? <RouteAuthentication {...props} /> : <Redirect to="/" />
            }
        />
    );
};

const CustomRoute = () => {
    return (
        <>
            <BrowserRouter>
                <Switch>
                    <Route exact name="login" path="/" component={Login} />

                    <AuthRoute
                        path="/"
                        render={(props) => <RouteAuthentication {...props} />}
                    />

                </Switch>
            </BrowserRouter>
        </>
    );
}

export default CustomRoute;
