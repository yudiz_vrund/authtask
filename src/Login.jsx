/* eslint-disable no-unused-vars */
import React, { useState, useRef } from 'react';
import './index.css';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Button } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import {Link } from 'react-router-dom';
import Dashboard from './Dashboard';


const Login = () => {

 
    const [email,setEmail] = useState("");

    const [password,setPassword] = useState("");

    const history = useHistory();
    

    let txtemail = useRef(null);
    let txtPassword = useRef(null);

    const handlerSubmit = () => {
        txtemail = email;
        txtPassword = password;
        if(txtemail === "vrundshah7490@gmail.com" && txtPassword === "Vrund@555")
        {
            localStorage.setItem('userData',txtemail);
            history.replace("/dashboard");
        }
        else
        {
            alert("Wrong Credentials");
        }
    }

    return (
        <>
            <AvForm className="loginForm" onSubmit={handlerSubmit}>
                <h4>Welcome</h4>

                <AvField type="email" placeholder="Email" name="email" value={email} 
                onChange={(e) => setEmail(e.target.value)}  validate={{ email: true }} />

                <AvField type="password" placeholder="Password" name="password" value={password}
                onChange={(e) => setPassword(e.target.value)}
                 validate={{
                    required: { value: true, errorMessage: 'Please enter a password' },
                    pattern: { value: '[a-zA-Z0-9!@#$%^*()]', errorMessage: 'Only with letter and numbers' },
                    minLength: { value: 8, errorMessage: 'Password must be between 8 and 16 characters' },
                    maxLength: { value: 16, errorMessage: 'Password must be between 8 and 16 characters' }
                }} />
                <Button size="lg" color="primary"  outline>
                    Login
                </Button>

            </AvForm>
            
        </>
    )
}

export default Login;