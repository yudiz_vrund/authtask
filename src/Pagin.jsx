import React from 'react';
import PropTypes from 'prop-types';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import './index.css';

const Pagin = ({ sportsPerPage, totalSports, paginate }) => {

    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalSports / sportsPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <>
            <div className="text">
            <Pagination>
                <PaginationItem>
                    <PaginationLink first href="#" />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink previous href="#" />
                </PaginationItem>
                {pageNumbers.map(number => (
                    <PaginationItem key={number}>
                        <PaginationLink onClick={() => paginate(number)} href="#">
                            {number}
                        </PaginationLink>
                    </PaginationItem>
                ))}
                <PaginationItem>
                    <PaginationLink next href="#" />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink last href="#" />
                </PaginationItem>
            </Pagination>
            </div>
            

        </>
    )
}


Pagin.propTypes = {
    sportsPerPage: PropTypes.number,
    totalSports: PropTypes.number,
    paginate: PropTypes.func
}

export default Pagin;